package main

import (
	"bufio"
	"context"
	"errors"
	"flag"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"regexp"
	"runtime"
	"time"

	"main/pool"

	"github.com/bitfield/script"
)

var (
	defaultRegExp            = `\s+[3-9][5-9][5-9][0-9]{4,}\s+`
	dirContainsFileToProcess = flag.String("dest", "./rawlogs", "specify the dir contains files to process")
	appendFile               = flag.String("gen", "./new/gen.log", "specify file abs path to extacted")
	processedFiles           = flag.String("done", "./new/processed.file", "specify file abs path to store processed file name")
	regexpStr                = flag.String("reg", defaultRegExp, "specify the regular expression to math")

	errDefault = errors.New("wrong argument type")
)

func main() {
	flag.Parse()
	flag.Usage = func() {
		w := flag.CommandLine.Output()
		fmt.Fprintf(w, "Usage of %s\n", os.Args)
		flag.PrintDefaults()
	}
	if len(os.Args) != 9 {
		flag.Usage()
		return
	}
	workercount := runtime.NumCPU() * 2
	WorkerPoolWithTimeout(workercount, *dirContainsFileToProcess, time.Hour*24)
}

// worker count = runtime.NumCPU()
func WorkerPoolWithTimeout(workerCount int, dirContainsRawlogs string, td time.Duration) {
	wp := pool.NewPool(workerCount)

	ctx, cancel := context.WithTimeout(context.TODO(), td)
	defer cancel()

	// generate jobs
	files, err := readFileList(dirContainsRawlogs)
	if err != nil {
		panic(err)
	}
	jobs, err := GenJobs(files)
	if err != nil {
		panic(err)
	}
	go wp.GenerateFrom(jobs)

	go wp.Run(ctx)

	for {
		select {
		case r, ok := <-wp.Results():
			if !ok {
				continue
			}
			if r.Err != nil && r.Err != context.DeadlineExceeded {
				log.Fatalf("worker pool error %s", r.Err)
				log.Printf("current go routines: %d", runtime.NumGoroutine())
			}
			if r.Err == nil && r.Value != nil {
				log.Printf("job %s done, go routines: %d\n", r.Descriptor.ID, runtime.NumGoroutine())
			}
		case <-wp.Done:
			log.Println("worker pool done.")
			return
		}
	}
}

// customize this func to get what you want
var ExecFunc = func(ctx context.Context, args interface{}) (interface{}, error) {
	f, ok := args.(string)
	if !ok {
		return nil, errDefault
	}
	if fileIsProcessed(f, *processedFiles) {
		log.Printf("file %s is processed, skip\n", f)
		return nil, nil
	} else {
		// process
		LinebyLineScan(f, *appendFile, *regexpStr)
		// add f to a file, when it's processed
		if err := appendProcessed(*processedFiles, f); err != nil {
			panic(err)
		}
	}

	return f, nil
}

//
func GetJobCount(dirContainsRawlogs string) []string {
	files, err := script.ListFiles(dirContainsRawlogs).Slice()
	if err != nil {
		panic(err)
	}
	return files
}

// GenJobs generate bunch of jobs
func GenJobs(fileSlice []string) ([]pool.Job, error) {
	if len(fileSlice) == 0 {
		return nil, errors.New("file slice to process is blank")
	}
	jobs := make([]pool.Job, len(fileSlice))
	for i := 0; i < len(fileSlice); i++ {
		jobs[i] = pool.Job{
			Descriptor: pool.JobDescriptor{
				ID:       pool.JobID(fileSlice[i]),
				JType:    pool.JobType(fmt.Sprintf("process %s", fileSlice[i])),
				Metadata: nil,
			},
			ExecFn: ExecFunc,
			Args:   fileSlice[i],
		}
	}
	return jobs, nil
}

// AbsPath take a relative path to current dir, returns the abs path of specified
func AbsPath(relPath string) string {
	dir, err := os.Getwd()
	if err != nil {
		panic(err)
	}
	joined := filepath.Join(dir, relPath)

	//
	if err := os.MkdirAll(filepath.Dir(joined), os.ModePerm); err != nil {
		panic(err)
	}
	return joined
}

func readFileList(dir string) ([]string, error) {
	files, err := script.ListFiles(dir).Slice()
	if err != nil {
		return nil, err
	}
	return files, nil
}

// LinebyLineScan extract logs we need
func LinebyLineScan(fileToProcess, fileToAppend, reg string) {
	var m, n int64
	file, err := os.Open(AbsPath(fileToProcess))
	if err != nil {
		panic(err)
	}
	defer file.Close()
	scanner := bufio.NewScanner(file)
	buf := make([]byte, 0, 128*1024)
	// set max buffer size to 2MB
	scanner.Buffer(buf, 4096*1024)

	fileHandle, err := os.OpenFile(AbsPath(fileToAppend), os.O_APPEND|os.O_RDWR|os.O_CREATE, 0777)
	if err != nil {
		panic(err)
	}
	defer fileHandle.Close()
	writer := bufio.NewWriterSize(fileHandle, 1024*4096)

	for scanner.Scan() {
		r := regexp.MustCompile(reg)
		if r.Match(scanner.Bytes()) {
			_, err := fmt.Fprintln(writer, scanner.Text())
			if err != nil {
				panic(err)
			}
			n++
			writer.Flush()
		}
		m++
		if m%10000 == 0 {
			fmt.Printf("file: %s, processed %d lines, matched %d lines.\n", fileToProcess, m, n)
		}
	}
	if err := scanner.Err(); err != nil {
		panic(err)
	}
}

//
func appendProcessed(f, line string) error {
	fileHandle, err := os.OpenFile(f, os.O_APPEND|os.O_RDWR|os.O_CREATE, 0777)
	if err != nil {
		log.Fatal(err)
	}
	defer fileHandle.Close()
	writer := bufio.NewWriterSize(fileHandle, 1024*4096)
	_, errp := fmt.Fprintln(writer, line)
	if errp != nil {
		panic(errp)
	}
	return writer.Flush()
}

//
func fileIsProcessed(fileName, processedFiles string) bool {
	file, err := os.OpenFile(AbsPath(processedFiles), os.O_RDWR|os.O_CREATE, 0666)
	if err != nil {
		fmt.Printf("open file %s error.", processedFiles)
		panic(err)
	}
	defer file.Close()
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		if fileName == scanner.Text() {
			return true
		}
	}
	return false
}
